#!/usr/bin/env python3

from input.interface import InputInterface as Input

def count_journals(url: str):
    inter = Input()
    pub = inter.get_publication(url)

    if pub.citations:
        for citation in pub.citations:
            journal = citation.journal
            if journal in cit:
                cit[journal] += 1
            else:
                cit[journal] = 1

    if pub.references:
        for reference in pub.references:
            journal = reference.journal
            if journal in cit:
                cit[journal] += 1
            else:
                cit[journal] = 1

if __name__ == "__main__":
    cit = {}
	
    count_journals("https://doi.org/10.1021/acs.jcim.1c00203")
    count_journals("https://doi.org/10.1021/acs.jcim.6b00561")
    count_journals("https://doi.org/10.1021/acs.jcim.6b00613")
    count_journals("https://doi.org/10.1021/acs.jcim.1c00917")
    count_journals("https://doi.org/10.1021/acs.jmedchem.0c01332")
		#count_journals("https://pubs.acs.org/doi/10.1021/acs.biochem.1c00290")
		#count_journals("https://pubs.acs.org/doi/10.1021/acsenvironau.1c00007")
		#count_journals("https://pubs.acs.org/doi/10.1021/acs.biochem.7b01162")

    cit = dict(sorted(cit.items(), key=lambda item: item[1]))
    for journal in cit:
        if journal != "":
            print(f'{journal}: {cit[journal]}')
