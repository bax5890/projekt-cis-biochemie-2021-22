import sys
from pathlib import Path
from verarbeitung.process_main import Processing
from verarbeitung.dev_files.print_graph_test import try_known_publications, try_delete_nodes


doi_list = []
doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.9b00249')
#doi_list.append('https://doi.org/10.1021/acs.jcim.9b00249')
doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.1c00203')
doi_list.append('https://doi.org/10.1021/acs.jmedchem.0c01332')
Processing(doi_list, 2, 2, 'test.json')
