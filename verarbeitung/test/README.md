# Projekt CiS-Projekt 2021/22

Directory to contain unittests for construction and update of publication graph

## Files in directory

input_test.py 

- 	Immitiert die Arbeit der Input Gruppe auf eine sehr einfache Weise.
	Beispielhafte Informationen werden aus Strings herausgelesen und als Klassenobjekt gespeichert.

construct_graph_unittest.py 

- 	Führt diverse Tests zur Konstruktion des Graphen ohne Vorkenntnisse mit eigenen Beispielen und 
	unserer Input_test Funktion aus.

update_graph_unittest.py

-	Führt diverse Tests zum Updaten eines alten Graphs mit aktualisierter Input Liste mit eigenen 
	Beispielen und unserer Input_test Funktion aus.

## Authors
- Donna Löding
- Alina Molkentin
- Xinyi Tang
- Judith Große
- Malte Schokolowski