import unittest

import sys  
from pathlib import Path

sys.path.append("../")

from verarbeitung.construct_new_graph.initialize_graph import init_graph_construction
from verarbeitung.construct_new_graph.export_to_json import output_to_json
from verarbeitung.update_graph.import_from_json import input_from_json
from verarbeitung.update_graph.update_graph import update_graph

class UpdatingTest(unittest.TestCase):
     maxDiff = None

     def test_import_from_json(self):
          nodes_old, edges_old = init_graph_construction(['doi_lg_1_i'],2,2,True)
          output_to_json(nodes_old, edges_old, test_var = True)
          nodes_new, edges_new = input_from_json('test_output.json')
          self.assertCountEqual(nodes_old,nodes_new)
          self.assertCountEqual(edges_old, edges_new)

     def test_deleted_input_dois(self):
          nodes_old_single, edges_old_single = init_graph_construction(['doi_lg_1_i'],2,2,True)
          nodes_old_both, edges_old_both = init_graph_construction(['doi_lg_1_i','doi_lg_2_i'],2,2,True)
          output_to_json(nodes_old_both, edges_old_both, test_var=True)
          nodes_new_single, edges_new_single = update_graph(['doi_lg_1_i'], 'test_output.json', 2, 2, True)
          self.assertCountEqual(nodes_old_single,nodes_new_single)
          self.assertCountEqual(edges_old_single, edges_new_single)

          nodes_old_single, edges_old_single = init_graph_construction(['doi_cg_i'],3,3,True)
          nodes_old_two, edges_old_two = init_graph_construction(['doi_lg_1_i','doi_cg_i'],3,3,True)
          nodes_old_three, edges_old_three = init_graph_construction(['doi_lg_1_i','doi_lg_2_i','doi_cg_i'],3,3,True)

     def test_new_height(self):
          nodes_height_0, edges_height_0 = init_graph_construction(['doi_lg_1_i'],2,0,True)
          nodes_height_1, edges_height_1 = init_graph_construction(['doi_lg_1_i'],2,1,True)
          nodes_height_2, edges_height_2 = init_graph_construction(['doi_lg_1_i'],2,2,True)

          output_to_json(nodes_height_2, edges_height_2, 'new_height.json', True)
          nodes_new_height_1, edges_new_height_1 = update_graph(['doi_lg_1_i'], 'new_height.json', 2, 1, True)
          self.assertCountEqual(nodes_height_1, nodes_new_height_1)
          self.assertCountEqual(edges_height_1, edges_new_height_1)

          nodes_height_2, edges_height_2 = init_graph_construction(['doi_lg_1_i'],2,2,True)
          output_to_json(nodes_height_2, edges_height_2, 'new_height.json', True)
          nodes_new_height_0, edges_new_height_0 = update_graph(['doi_lg_1_i'], 'new_height.json', 2, 0, True)
          self.assertCountEqual(nodes_height_0, nodes_new_height_0)
          self.assertCountEqual(edges_height_0, edges_new_height_0)





          



def keep_only_dois(nodes):
     '''
          :param nodes:  input list of nodes of type Publication
          :type nodes:   List[Publication]

          gets nodes of type pub and return only their doi
     '''
     doi_list = []
     for node in nodes:
          doi_list.append(node.doi_url)
     return doi_list


if __name__ == "__main__":
     unittest.main()