# -*- coding: utf-8 -*-
"""
Functions to test and print the nodes and edges sets

"""

__authors__ = "Donna Löding, Alina Molkentin, Xinyi Tang, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Production"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""


import sys

#sys.path.insert(1, 'C:\Users\Malte\Git\CiS-Projekt\ci-s-projekt-verarbeitung\input')
sys.path.append("../../")
from verarbeitung.construct_new_graph.initialize_graph import init_graph_construction
from verarbeitung.update_graph.import_from_json import input_from_json
from verarbeitung.update_graph.update_graph import update_graph

# a function to print nodes and edges from a graph
def print_graph(nodes, edges):
    print("Knoten:\n")
    for node in nodes:
        print(node.title, "\n")
    print("\nKanten:\n")
    for edge in edges:
        print(edge,"\n")
    print(len(nodes))
    print(len(edges))
    print(" ")

def print_extended_graph(nodes, edges):
    print("Knoten:\n")
    for node in nodes:
        print(node.title, "\n")
        print(node.doi_url)
        for reference in node.references:
            print(reference.doi_url)
        for citation in node.citations:
            print(citation.doi_url)
    print("\nKanten:\n")
    for edge in edges:
        print(edge,"\n")
    print(len(nodes))
    print(len(edges))
    print(" ")

def print_simple(nodes, edges):
    # for node in nodes:
    #     print(node)
    # for edge in edges:
    #     print(edge)
    print(len(nodes))
    print(len(edges))
    print(" ")

# program test with some random dois
def try_known_publications():
    doi_list = []
    doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.9b00249')
    #doi_list.append('https://doi.org/10.1021/acs.jcim.9b00249')
    doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.1c00203')
    #arr.append('https://pubs.acs.org/doi/10.1021/acs.jcim.9b00249')
    doi_list.append('https://doi.org/10.1021/acs.jmedchem.0c01332')
    #arr.append('https://doi.org/10.1021/acs.jcim.0c00741')

    #arr.append('https://doi.org/10.1021/ci700007b')
    #doi_list.append('https://doi.org/10.1021/acs.jcim.5b00292')
    
    #doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.0c00675')
    #url = sys.argv[1]
    #arr.append[url]


    nodes, edges = init_graph_construction(doi_list,2,2)

    print_graph(nodes, edges) 

    return(nodes, edges)

def try_delete_nodes():
    doi_list = []
    doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.9b00249')
    #doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.1c00203')
    nodes, edges = init_graph_construction(doi_list,1,1)
    #print_simple(nodes, edges)

    # list_of_nodes_py, list_of_edges_py = input_from_json('json_text.json')
    # doi_list = []
    # doi_list.append('https://pubs.acs.org/doi/10.1021/acs.jcim.9b00249')
    # valid_nodes, valid_edges = update_graph(doi_list, list_of_nodes_py, list_of_edges_py)
    # print_simple(valid_nodes, valid_edges)

def try_import():
    nodes, edges = input_from_json('json_text.json')
    print_extended_graph(nodes,edges)

#nodes, edges = try_known_publications()
#nodes_new, edges_new = input_from_json("json_text.json")
#print_graph(nodes_new, edges_new)
try_delete_nodes()

#try_import()