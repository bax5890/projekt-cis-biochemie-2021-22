# -*- coding: utf-8 -*-
"""
Functions to update the citation depth of recursive graph construction

"""

__authors__ = "Donna Löding, Alina Molkentin, Xinyi Tang, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Production"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""

import sys
sys.path.append("../../")

from verarbeitung.construct_new_graph.add_citations_rec import add_citations
from verarbeitung.construct_new_graph.initialize_graph import complete_inner_edges
from .update_edges import back_to_valid_edges


def reduce_max_height(max_height):
    '''
        :param max_height:        new maximum height to reduce publications in publication list to
        :type max_height:         int

        function to remove all publications which are not in new maximum height threshold
    '''
    input_list_del = processed_input_list.copy()
    for pub in input_list_del:
        if (pub.group > 0):
            if (pub.group > max_height):
                processed_input_list.remove(pub)

def reduce_max_depth(max_depth):
    '''
        :param max_depth:   new maximum depth to reduce publications in publication list to
        :type max_depth:    int

        function to remove all publications which are not in new maximum depth threshold
    '''
    input_list_del = processed_input_list.copy()
    for pub in input_list_del:
        if (pub.group < 0):
            if (abs(pub.group) > max_depth):
                processed_input_list.remove(pub)



def get_old_height_depth():
    '''
        function to get old max height and max depth from previous construction call
    '''
    max_height = 0 
    max_depth = 0
    for pub in processed_input_list:
        if (pub.group < 0):
            max_depth = max(max_depth, abs(pub.group))
        if (pub.group > 0):
            max_height = max(max_height, pub.group)
    return(max_height, max_depth)

def get_old_max_references(old_depth):
    '''
        :param old_depth:       old maximum depth to search for citations
        :type old_depth:        int

        function to get references for new recursive levels
    '''
    old_max_references = []
    for pub in processed_input_list:
        if (abs(pub.group) == old_depth):
            for reference in pub.references:
                for ref_pub in processed_input_list:
                    if reference.doi_url == ref_pub.doi_url:
                        old_max_references.append(ref_pub)
    return(old_max_references)

def get_old_max_citations(old_height):
    '''
        :param old_height:      old maximum height to search for citations
        :type old_height:       int

        function to get citations for new recursive levels
    '''
    old_max_citations = []
    for pub in processed_input_list:
        if (abs(pub.group) == old_height):
            for citation in pub.citations:
                for cit_pub in processed_input_list:
                    if citation.doi_url == cit_pub.doi_url:
                        old_max_citations.append(cit_pub)
    return(old_max_citations)

def update_depth(obj_input_list, input_edges, new_depth, new_height, test_var):
    '''
        :param obj_input_list:  input list of publications of type Publication from update_graph
        :type obj_input_list:   List[Publication]

        :param input_edges:     list of publications from update_graph
        :type input_edges:      List[Publication]

        :param new_depth:       new maximum depth to search for references
        :type new_depth:        int

        :param new_height:      new maximum height to search for citations
        :type new_height:       int

        :param test_var:        variable to differenciate between test and url call
        :type test_var:         boolean

        function to adjust old publication search depth to update call
    '''

    global processed_input_list, valid_edges
    processed_input_list = obj_input_list
    valid_edges = input_edges

    old_height, old_depth = get_old_height_depth()

    # removes publications and links from recursion levels which aren't needed anymore
    if (old_depth > new_depth):
        reduce_max_depth(new_depth)
    elif (old_height > new_height):
        reduce_max_height(new_height)
    
    
    # adds publications and links for new recursion levels
    elif (old_depth < new_depth):
        old_max_references = get_old_max_references(old_depth)
        add_citations(processed_input_list, valid_edges, old_max_references, old_depth+1, new_depth, "Reference", test_var)
    elif (old_height < new_height):
        old_max_citations = get_old_max_citations(old_height)
        add_citations(processed_input_list, valid_edges, old_max_citations, old_height+1, new_height, "Citation", test_var)
    back_to_valid_edges(valid_edges, processed_input_list)

    # adds edges between reference group and citation group of known publications

    return(old_depth, old_height)


    

