#!/usr/bin/env python3
from collections import Counter

def compare_old_and_new_node_lists(old_doi_node_list,new_doi_node_list):
    '''
    :param old_doi_node_list:   list of dois from old graph
    :type old_doi_node_list:    List[String]
    
    :param new_doi_node_list:   list of dois from new graph
    :type new_doi_node_list:    List[String]
    
    function to calculate, which nodes from the old graph are deleted and which are added
    '''
    dois_from_old_graph = old_doi_node_list #WICHTIG: Keine doppelten DOIs
    dois_from_new_graph = new_doi_node_list
    deleted_nodes = []
    common_nodes = []
    inserted_nodes = []
    all_dois = dois_from_old_graph + dois_from_new_graph

    for doi in all_dois: # iterates over the merged list of new and old dois 
        if ((all_dois.count(doi) == 2) & (doi not in common_nodes)): # If the doi occurs twice the node is in the old and the new graph
            common_nodes.append(doi) #appends the doi to common ones, if its not alredy in it
        elif ((doi in dois_from_old_graph) & (doi not in dois_from_new_graph)): #If the doi occurs once and it is from old graph it is a deleted node
            deleted_nodes.append(doi) #appends the doi to deleted ones
        elif ((doi in dois_from_new_graph) & (doi not in dois_from_old_graph)): #if the doi occurs ince and it is from new graph it is a inserted node
            inserted_nodes.append(doi) #appends the doi to the inserted ones
    return(common_nodes, inserted_nodes, deleted_nodes)


#Test Prints
	#liste_1 = ["doi_1","doi_2","doi_3","doi_4","doi_5"]
	#liste_2 = ["doi_1","doi_2","doi_3","doi_6","doi_7"]
	#print("gemeinsame Elemente: ",doi_listen_vergleichen(liste_1,liste_2)[0])
	#print("hinzugefügte Elemente: ",doi_listen_vergleichen(liste_1,liste_2)[1])
	#print("gelöschte Elemente: ",doi_listen_vergleichen(liste_1,liste_2)[2])

