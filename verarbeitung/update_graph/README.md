# Projekt CiS-Projekt 2021/22

Directory for functions to adjust a publication graph to updated input lists and changed citation/reference depths. For minimal use of the time consuming Input function, a reinterpretation of the exported json file is implemented.

## Files in directory

import_from_json.py 

- 	Stellt die alte Knoten-und Kantenmenge aus der Json Datei wieder her.

compare_old_and_new_lists.py 

- 	Überprüft welche Knoten neu hinzugekommen sind und welche enfternt wurden.

update_edges.py 

- 	Stellt nach der Löschung eines Knotens wieder eine valide Kantenmenge her.

delete_nodes_edges.py 

- 	Führt die Löschung eines Input-Knotens und seiner Verwandten durch

connect_new_input.py 

- 	Verbindet den alten Graphen aus der Json Datei mit den neuen DOIs zu dem neuen Graphen.

update_graph.py 

- 	Überprüft welche Änderungen der Benutzer vorgenommen hat (Löschen oder hinzufügen von DOIs) 
	und führt diese aus.

update_depth.py

-	aktualisiert die Höhe/Tiefe des vorhandenen Graphen auf neue Input Höhe/Tiefe

## Authors
- Donna Löding
- Alina Molkentin
- Xinyi Tang
- Judith Große
- Malte Schokolowski