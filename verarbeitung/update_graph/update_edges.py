#!/usr/bin/env python3

def back_to_valid_edges(links_from_json, processed_input_list):
    '''
    :param links_from_json: list of edges from the old graph
    :type links_from_json:  list
    :param processed_input_list:   list pubs still in graph
    :type processed_input_list:    list
    
    function that deletes edges, if one ore two including nodes are deleted nodes
    '''
    list_of_valid_edges = links_from_json.copy()


    #iterates over all edges from old graph
    for edge in list_of_valid_edges:

        # counter for adjacent nodes
        found_adj_nodes = 0
        for pub in processed_input_list: 
            # checks for both adjacent nodes of edge if pub is source/target node
            for adj_node in edge:
                # increases counter if adjacent node was found
                if (adj_node == pub.doi_url):
                    found_adj_nodes += 1
            if (found_adj_nodes == 2):
                break

        #removes the edge if less than 2 adjacent nodes found
        if (found_adj_nodes < 2):
            links_from_json.remove(edge) 

#Kanten_Menge_Ganz = [["doi_1","doi_2"],["doi_3","doi_4"],["doi_5","doi_6"]]
#Geloeschte = ["doi_2","doi_1","doi_4"]
#print(back_to_valid_edges(Kanten_Menge_Ganz,Geloeschte))

#Im Anschluss muss mit den Hinzugefügten Knoten Processing aufgerufen werden
