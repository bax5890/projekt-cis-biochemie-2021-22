# -*- coding: utf-8 -*-
"""
A function to return an object of Type Publication for a given doi

"""

__authors__ = "Donna Löding, Alina Molkentin, Xinyi Tang, Judith Große, Malte Schokolowski"
__email__ = "cis-project2021@zbh.uni-hamburg.de"
__status__ = "Production"
#__copyright__ = ""
#__credits__ = ["", "", "", ""]
#__license__ = ""
#__version__ = ""
#__maintainer__ = ""


import sys  
from pathlib import Path
sys.path.append("../")

from input.interface import InputInterface as Input
from verarbeitung.test.input_test import input_test_func


def get_pub(pub_doi, test_var):
    '''
        :param pub_doi:  input doi to get Publication object for
        :type pub_doi:   String

        :param test_var:        variable to differenciate between test and url call
        :type test_var:         boolean

        function to return an object of type Publication for given input doi depending on whether its a test or url doi
    '''
    #checks if it's a test and chooses appropiate function
    if(test_var): 
        pub = input_test_func(pub_doi) 

    #checks that it isnt a test and chooses standart-input function
    else: 
        inter = Input()
        try:
            pub = inter.get_publication(pub_doi) #creates an object of class Publication
        except AttributeError:
            pub = inter.get_publication(pub_doi)
        except ValueError:
            return(ValueError)
        except IndexError:
            return(IndexError)
    return(pub)