# Projekt CiS-Projekt 2021/22

Directory for functions to create the fundamental graph structure at first time call of programm.

## Files in directory

initialize_graph.py 

- 	Führt den grundlegendem Graphbauprozess aus. Die Input-DOIs werden 
	als Klassenobjekt zur Knotenmenge hinzugefügt und über einen rekursiven Aufruf
	wird die angegene Zitierungstiefe in beide Richtungen zu den Kanten hinzugefügt.


add_citations_rec.py 

- 	Die DOIs, die in den Zitierungen des Inputs zu finden sind, werden ebenfalls zu Knoten
	und je nach angegebener Höhe oder Tiefe wird dies für weitere Tiefen erneut ausgeführt.


export_to_json.py 

- 	Wandelt die berechnete Knoten- und Kantenmenge in eine Json Datei um.

## Authors
- Donna Löding
- Alina Molkentin
- Xinyi Tang
- Judith Große
- Malte Schokolowski