#!/usr/bin/env python3

from input.interface import InputInterface as Input

def main(url: str):
    i = Input()
    #print(i.get_publication(url))
    print(i.get_pub_light(url))
    # print(i.get_supported_fetchers()) Useless because all classes are called the same

if __name__ == "__main__":
	#main("https://doi.org/10.1021/acs.jcim.1c0023")
    main("https://doi.org/10.1021/acs.jcim.5b00332")
