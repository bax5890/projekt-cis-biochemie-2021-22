# Projekt CiS-Projekt 2021/22

Citation network made with **d3.js**

## Usage
### Input 
Json file **json\_text.json** in directory
```json
{
    "nodes": [
        {
            "name": <title: string>,
            "author": [<author1: string>, <author2: string>, ...],
            "year": <date: tring>,
            "journal": <journal: string>,
            "doi": <doi: string>,
            "group": <"input"/"height"/"depth">,
            "citations": <citation: int>
        }, ...
    ],
    "links": [
        {
            "source": <doi: string>,
            "target": <doi: string>
        }, ...
    ]
}
```

### Display the Citation Network
Starting a python web server:
```sh
   cd <path to file> &&python3 -m http.server <port>
```
Access to the server:
[http://0.0.0.0/:\<port\>](http://0.0.0.0/:<port>)

## Files in Directory
- **index.html**: webpage
- **cn.js**: javascript code for force-directed graph, text elements and legend
- **json_text.json**: example data


## Authors
- Katja Ehlers
- Merle Stahl