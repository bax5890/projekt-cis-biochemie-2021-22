#!/usr/bin/env python3

from input_fj import input, print_pub_info
import sys

if len(sys.argv) != 3:
    sys.stderr.write('Usage: {} <url> <url>\n'.format(sys.argv[0]))
    exit(1)
url = sys.argv[1]
url2 = sys.argv[2]
pub = input(url)
print_pub_info(pub)
pub2 = input(url2)
print_pub_info(pub2)

