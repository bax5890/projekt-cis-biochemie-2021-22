class Publication:
    #_registry = []
    #_citations = []
    #_references = []
    
    def __init__(self, title, publication_date, contributors, doi_url, 
                 subjects, num_citations):
        #self._registry.append(self)
        self.title = title
        self.publication_date = publication_date
        self.contributors = contributors
        self.doi_url = doi_url
        self.subjects = subjects
        self.num_citations = num_citations
        self.num_references = num_references
    	self._citations = []
    	self._references = []

class Citation:
    def __init__(self, title, journal, contributors, doi_url):
        self.title = title
        self.journal = journal
        self.contributors = contributors
        self.doi_url = doi_url

class References:
    def __init__(self, title, journal, contributors, doi_url):
        self.title = title
        self.journal = journal
        self.contributors = contributors
        self.doi_url = doi_url

