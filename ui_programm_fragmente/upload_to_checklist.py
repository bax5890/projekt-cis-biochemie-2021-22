import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
import base64
import re

app = dash.Dash(__name__)

list_of_inputs = dict()

app.layout = html.Div([
    html.H4("Add all lines in a file to a list"),
    html.Div([
        dcc.Upload(
            id="upload-data",
            children=html.Div(
                ["Drag and drop or click to select a file to upload."]
            ),
            
            style={
                "width": "30%",
                "height": "60px",
                "lineHeight": "60px",
                "borderWidth": "1px",
                "borderStyle": "dashed",
                "borderRadius": "5px",
                "textAlign": "center",
                "margin": "10px",
            }),

    ]),
    dcc.Checklist(id='input-checklist',options=list(),labelStyle = dict(display='block'),value=[]),

])

@app.callback(
    Output('input-checklist','options'),
    Input('upload-data','filename'),
    Input('upload-data','contents'),
    State('input-checklist','options')
)
def update_input_list(uploaded_filenames,uploaded_file_contents,all_inputs):
    if uploaded_file_contents is not None:
  
            
        string = uploaded_file_contents

        #cutting the first part of the String away to decode
        found = base64.b64decode(re.search(',(.+?)$', string).group(1))
        print(found.decode('utf-8'))
        
        uploaded_file_contents = found.decode('utf-8')
        
    
        list_of_inputs = (uploaded_file_contents.split())
        #das hier sollte es untereinander anzeigen, bekomme ich allerdings nicht auf die Seite...
        #return (*list_of_inputs, sep="\n")
            
        options = all_inputs
        if not options:
            options = list()
        CurrValues = [x['value'] for x in options]

           
       # würde auch funktionieren
       # return (found.decode('utf-8'))
        for i in list_of_inputs:
            if i not in CurrValues:
                options.append({'label':i, 'value':i})


        return options

if __name__ == '__main__':
    app.run_server(debug=True)


