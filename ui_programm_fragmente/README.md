# Projekt CiS-Biochemie 2021-22 UI

# Benötigt:
- Dash 
- Pandas
- beautifulsoup4
- requests

# Starten des Programms:

Ausführen von citation_parser_ui.py und einfügen des entstandenen Liks in einen Browser.
Danach müsste sich die Benutzeroberfläche im Browser öffnen.


# Übersicht der Benutzeroberfläche:

- Show Info: Durch wiederholtes klicken kann das Fenster ein und aus geblendet werden.

- Input: Die Eingabe erfolgt in Form eines DOI ("Digital Object Identifier") 

- Drag and drop or click to select a file to upload: Mehrere DOI in einem txt-Dokument (genau ein DOI pro Zeile).

- Recursion: die beiden noch unbeschrifteten Felder rechts neben Input sind für die Rekursionstiefen in beide Richtungen

- Clear All: alle Eingaben werden gelöscht

- Clear Selected: alle markierten Eingaben werden gelöscht

- Generate Graph: generiert den zugehörigen Graphen (generiert momentan nur einen string)

- Update Automatically: automatische Aktualisierung des Graphen bei jeder neuen Eingabe

- Smart Input: direkte Überprüfung der Eingabe auf Richtigkeit zudem wird nicht mehr der DOI angezeigt sondern: 
  Der Autor, Das Journal, Das Veröffentlichungsdatum. (muss vor Hinzufügen aktiviert worden sein)

## Autoren
- Isabelle Siebels
- Sebastian David
