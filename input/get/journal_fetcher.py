#!/usr/bin/env python3

"""
Parent class for specific Journal
"""

from abc import ABCMeta, abstractmethod
from bs4 import BeautifulSoup
import requests
from input.publication import Publication


class JournalFetcher(metaclass=ABCMeta):
    """
    This is a abstract-class for fetcher modules
    """
    
    @staticmethod
    def get_soup(url: str) -> BeautifulSoup:
        """
        Retrieves webside-html and returns a BeautifulSoup-instance

        Parameters:
        -----------
        :type url: str
        :param url: doi-url to a publication
        :return: BeatifulSoup-instance
        """
        try:
            req = requests.get(url)
        except  requests.exceptions.HTTPError as err:
            raise SystemExit(err)

        return BeautifulSoup(req.content, 'html.parser')


    @staticmethod
    @abstractmethod
    def can_use_url(url: str) -> bool:
        """
        Abstract-function to be implemented in subclass.
        Checks if given url links to a supported journal
        """
        raise AttributeError("JournalFetcher for '{}' hasnt implemented 'can_use_url()'".format(url))


    @staticmethod
    @abstractmethod
    def get_publication(url: str) -> Publication:
        """
        Abstract-function to be implemented in subclass.
        Creates a Publication-instance.
        """
        raise AttributeError("JournalFetcher for '{}' hasnt implemented 'get_publication()'".format(url))


    # A Dictionary, which connects abbreviation to whole journal-name
    abbrev_dict = {
          "Nat. Protoc.":"Journal of Natural Products"
        ,"PLoS Comput. Biol.":"PLoS Computational Biology"
        ,"PLoS One":"PLoS One"
        ,"Protein Sci.":"Protein Science"
        ,"J. Am. Chem. Soc.":"Journal of the American Chemical Society"
        ,"J. Chem. Phys.":"Journal of Chemical Physics"
        ,"Appl. Sci.":"Applied Science"
        ,"Comput. Sci. Eng.":"Computing in Science & Engineering"
        ,"Beilstein J. Org. Chem.":"Beilstein Journal of Organic Chemistry"
        ,"Biol. Chem.":"Biological Chemistry"
        ,"Isr. J. Chem.":"Israel Journal of Chemistry"
        ,"Nat. Methods":"Nature Methods"
        ,"Proc. Natl. Acad. Sci. U. S. A.":"Proceedings of the National Academy of Sciences of the United States of America"
        ,"J. Phys. Chem. B":"Journal of Physical Chemistry B"
        ,"Carbohydr. Res.":"Carbohydrate Research"
        ,"J. Chem. Theory Comput.":"Journal of Chemical Theory and Computation"
        ,"J. Mol. Biol.":"Journal of Molecular Biology"
        ,"Nucleic Acids Res.":"Nucleic Acids Research"
        ,"J. Comput. Chem.":"Journal of Computational Chemistry"
        ,"J. Cheminf.":"Journal of Cheminformatics"
        ,"J. Med. Chem.":"Journal of Medicinal Chemistry"
        ,"J. Comput.-Aided Mol. Des.":"Journal of Computer-Aided Molecular Design"
        ,"J. Chem. Inf. Model.":"Journal of Chemical Information and Modeling"
        ,"Mol. Cell":"Molecular Cell"
        ,"J. Cell Biolog.":"Journal of Cell Biology"
        ,"Mol. Cell Biol.":"Molecular and Cellular Biology"
        ,"J. Cell Sci.":"Journal of Cell Science"
        ,"Nat. Cell Biol.":"Nature Cell Biology"
        ,"J. Aerosol Sci. Technol.":"Aerosol Science and Technology"
        ,"Mol. Biol. Cell":"Molecular Biology of the Cell"
        ,"Build. Environ.":"Building and Environment"
        ,"Sci. Rep.":"Scientific Reports"
        ,"Nat. Chem.":"Nature Chemistry"
        ,"Nat. Med.":"Nature Medicine"
        ,"Nat. Commun.":"Nature Communications"
        ,"Exp. Cell Res.":"Experimental Cell Research"
        ,"Nat. Chem. Biol.":"Nature Chemical Biology"
        }