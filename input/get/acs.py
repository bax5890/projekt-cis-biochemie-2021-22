#!/usr/bin/env python3

"""
Child class of JournalFetcher
Usage: Check if Url can be used with 'can_use_url'
       and then fetch publication with 'get_publication'
"""

import re

from input.get.journal_fetcher import JournalFetcher
from input.publication import Publication, Citation


class Fetcher(JournalFetcher):
    """
    Specific Fetcher for the ACS journals.
    """

    # Constant for the abbreviations of the supported Journals
    SUPPORTED_JOURNALS = ['1021']

    @staticmethod
    def can_use_url(url: str) -> str:
        """
        Uses Regex to extract journal specific substrings in Doi.
        TODO: Support non Doi-urls
        """
        matched_url = re.match(r'^(https?://)?(doi.org/|pubs.acs.org/doi/)?(10.(\d{4})/\w+.\S+)', url.strip(". \t\r\n"))
        
        #Checks if match exists
        if matched_url is not None:
            return matched_url[4] in Fetcher.SUPPORTED_JOURNALS
        else:
            return False

    @staticmethod


    def get_pub_light(url: str) -> Publication:
        """
        Fetches html and creates Beatifulsoup-instance in parent class.
        Specific css-searches for ACS-Journals and creates Publication-instance.
        """

        # Creation of Soup
        try:
            soup = JournalFetcher.get_soup(url)
        except Exception as error:
            raise error
        
        # Raise Error if re recognizes Pattern, but url isnt correct:
        #   For other Urls
        if soup.text.strip(" \t\n")=="Missing resource null":
            raise ValueError("'{}' matches Pattern for 'ACS', but doesnt link to Paper.".format(url))

        #   For Dois
        if soup.title is not None:
            if soup.title.text == "Error: DOI Not Found":
                raise ValueError("'{}' matches Pattern for 'ACS', but doesnt link to Paper.".format(url))

        
        soup_header = soup.select('.article_header')[0]
        
        # Creates Publication
        doi_url = soup_header.select('a[title="DOI URL"]')[0].string
        title = soup_header.select(".hlFld-Title")[0].text

        contributors = []
        for author in soup_header.select(".hlFld-ContribAuthor"):
            contributors.append(author.text)

        journal = soup_header.select(".cit-title")[0].text

        # Replaces abbreviation with whole name
        if journal in JournalFetcher.abbrev_dict:
            journal = JournalFetcher.abbrev_dict[journal]
                

        published = soup_header.select(".pub-date-value")[0].text

        subjects = []
        subject_soup = soup_header.select('.article_header-taxonomy')[0]
        for subject in subject_soup.select('a'):
            subjects.append(subject.text)

        return Publication(doi_url, title, contributors, journal, published, 
                           subjects)

    def get_publication(url: str) -> Publication:
        """
        Fetches html and creates Beatifulsoup-instance in parent class.
        Specific css-searches for ACS-Journals and creates Publication-instance.
        """

        # Creation of Soup
        try:
            soup = JournalFetcher.get_soup(url)
        except Exception as error:
            raise error
        
        # Raise Error if re recognizes Pattern, but url isnt correct:
        #   For other Urls
        if soup.text.strip(" \t\n")=="Missing resource null":
            raise ValueError("'{}' matches Pattern for 'ACS', but doesnt link to Paper.".format(url))

        #   For Dois
        if soup.title is not None:
            if soup.title.text == "Error: DOI Not Found":
                raise ValueError("'{}' matches Pattern for 'ACS', but doesnt link to Paper.".format(url))

        
        soup_header = soup.select('.article_header')[0]
        
        #Could be used for more specific search
        ref_cit_soup = soup

        # Creates Publication
        doi_url = soup_header.select('a[title="DOI URL"]')[0].string
        title = soup_header.select(".hlFld-Title")[0].text

        contributors = []
        for author in soup_header.select(".hlFld-ContribAuthor"):
            contributors.append(author.text)

        journal = soup_header.select(".cit-title")[0].text

        # Replaces abbreviation with whole name
        if journal in JournalFetcher.abbrev_dict:
            journal = JournalFetcher.abbrev_dict[journal]
                

        published = soup_header.select(".pub-date-value")[0].text

        subjects = []
        subject_soup = soup_header.select('.article_header-taxonomy')[0]
        for subject in subject_soup.select('a'):
            subjects.append(subject.text)


        references = []
        references_soup = ref_cit_soup.select('ol#references')
        if references_soup != []:
            for reference in references_soup[0].select('li'):
                if reference.select('.refDoi') != []:
                    ref_doi = "https://doi.org/{}".format(reference.select('.refDoi')[0].text.strip()[5:])
                else: 
        #           No Doi -> No Paper
                    continue
                ref_title = reference.select('.NLM_article-title')[0].text\
                        if reference.select('.NLM_article-title') != [] else None
                ref_journal = reference.select('i')[0].text\
                        if reference.select('i') != [] else None

                # Replaces abbreviation with whole name
                if ref_journal in JournalFetcher.abbrev_dict:
                    ref_journal = JournalFetcher.abbrev_dict[ref_journal]
                
                ref_contributors=[]
                for author in reference.select('.NLM_contrib-group'):
                    ref_contributors.append(author.text.replace("\n", " ").replace("\r", ""))

                references.append(Citation(ref_doi, ref_title, ref_journal, ref_contributors, cit_type="Reference"))

        citations = []
        citation_soup = ref_cit_soup.select('.cited-content_cbyCitation')
        if citation_soup != []:
            for citation in citation_soup[0].select('li'):
                if citation.select('a[title="DOI URL"]') != []: 
                    cit_doi = citation.select('a[title="DOI URL"]')[0].text
                else:
        #           No Doi -> No Paper
                    continue
                cit_title = citation.select('.cited-content_cbyCitation_article-title')[0].text\
                        if citation.select('.cited-content_cbyCitation_article-title')!= [] else None
                cit_journal = citation.select('.cited-content_cbyCitation_journal-name')[0].text\
                        if citation.select('.cited-content_cbyCitation_journal-name') != [] else None

                # Replaces abbreviation with whole name
                if cit_journal in JournalFetcher.abbrev_dict:
                    cit_journal = JournalFetcher.abbrev_dict[cit_journal]
                cit_contributors =[]
                cit_contributors = citation.select('.cited-content_cbyCitation_article-contributors')[0]\
                    .text.replace("\n", " ").replace("\r", "").split(', ')
        #           clean up of the last Entry
                cit_contributors_last = cit_contributors.pop().strip(". ")
                if cit_contributors_last != '':
                    cit_contributors.append(cit_contributors_last)  
                citations.append(Citation(cit_doi, cit_title, cit_journal, cit_contributors, cit_type = "Citation"))

        return Publication(doi_url, title, contributors, journal, published
                            , subjects, references, citations)
