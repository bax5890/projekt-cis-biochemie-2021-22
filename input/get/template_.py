#!/usr/bin/env python3

"""
Child class of JournalFetcher
Usage: None, this is just a template and should be ignored
"""

# import re
from input.get.journal_fetcher import JournalFetcher
from input.publication import Publication


class Fetcher(JournalFetcher):

    """
    This is only a template and therefore has no functionality
    """

    # TODO: Naming-Convention:
    #   Class: 'Fetcher'
    #   file: [journal-/organisation-name]
    #       format = "[a-z]*.py" allowed
    #   TODO: List of Compatable Journals
    SUPPORTED_JOURNALS = []

    @staticmethod
    def can_use_url(url: str) -> bool:
        """
        Checks if given url links to a supported journal.
        """

        # TODO: Check the URL for compatability
        #   url_re = re.match(r'(https?://)?(doi.org/)?(10.(\d{4})/\w+.\S+)', url)
        #   if url_re is not None:
        #       return   url_re[4] in SUPPORTED_JOURNALS
        #   else:
        return False

    @staticmethod
    def get_publication(url: str) -> Publication:
        """
        Creates a Publication-instance.
        """

        # TODO: Fetch data from the HTML
        #   soup = JournalFetcher.get_soup(url)
        #   doi,title,contributors[],journal,publication_date,subjects[],references[],citations[] 
        # TODO: Create new Publication-instance
        #   return Publication(doi_url, title, contributors = [], journal
        #           , publication_date, subjects = [], references = [], citations = [])
        return None