#!/usr/bin/env python3

"""
Child class of JournalFetcher
Usage: Check if Url can be used with 'can_use_url'
       and then fetch publication with 'get_publication'
"""

# import re
from input.get.journal_fetcher import JournalFetcher
from input.publication import Publication


class Fetcher(JournalFetcher):

    """
    scrapes publication metadata from a provided url
    """

    #   TODO: List of Compatable Journals
    #   NOTE: nature does not use journal names in doi links, must match by 10.xxxx identifier instead
    SUPPORTED_JOURNALS = []

    @staticmethod
    def can_use_url(url: str) -> bool:
        """
        Checks if given url links to a supported journal.
        """

        # TODO: Check the URL for compatability
        #   re.match in SUPPORTED_JOURNALS
        return False

    @staticmethod
    def get_publication(url: str) -> Publication:
        """
        Creates a Publication-instance.
        """

        soup = JournalFetcher.get_soup(url)

        _doi_url = "https://doi.org/" + soup.head.find(attrs={"name": "DOI"}).get("content")
        _title = soup.head.find(attrs={"name": "citation_title"}).get("content")
        _journal = soup.head.find(attrs={"name": "citation_journal_title"}).get("content")
        _published = soup.head.find(attrs={"name": "prism.publicationDate"}).get("content")
        _contributors = []
        _subjects = []

        for creator in soup.head.findAll(attrs={"name": "dc.creator"}):
            _contributors.append(creator.get("content"))

        for subject in soup.head.findAll(attrs={"name": "dc.subject"}):
            _subjects.append(subject.get("content"))

        return Publication(_doi_url, _title, _contributors, _journal, _published, _subjects)

        # TODO: Exceptions-handling
        #   raise ValueException("Cant Fetch: '{}'".format(error))
        # return None
