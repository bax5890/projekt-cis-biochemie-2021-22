#!/usr/bin/env python3

"""
Interface for the Input-Package only this should be accessed from outside this Package.

"""
from os import walk
import importlib
import pathlib
import re
from input.publication import Publication

class InputInterface:
    """
    Singleton which dynamically imports and manages fetchers
    """

    instance = None
    get_path = None
    fetcher_classes=[]

    # '__new__' is called before '__init__' and gives us an instance
    def __new__(cls, *args, **kwargs):
        
        # checks if an instance exists and if it doesnt creates one
        if cls.instance == None:
            cls.instance = super(InputInterface, cls).__new__(cls,*args, **kwargs)
        
        return cls.instance

    def __init__(self):
        # imports all modules

        if self.fetcher_classes ==[]:
            self.import_fetcher_classes()
            if self.fetcher_classes ==[]:
                raise AttributeError("No specific Fetchers where found at: '{}'"
                                    .format(self.get_path))
        

    def get_publication(self, url: str) -> Publication:
        """
        The interface-method to get a Publication-instance
        (including it's citations and references)

        Parameters
        ----------
        :param url: url to a Publication
        :type url: str
        :return: Publication instance or None if not supported
        """
        
        # Checks if module supports the 'url' and 
        # returns a Publication if it does.
        for fetcher_class in InputInterface.fetcher_classes:
            if fetcher_class.can_use_url(url):
                return fetcher_class.get_publication(url)
            
        # No Module for given url was found
        raise ValueError("'{}' is not supported".format(url))
        
    def get_pub_light(self, url: str) -> Publication:
        """
        The interface-method to get a Publication-instance 
        (only for main article)

        Parameters
        ----------
        :param url: url to a Publication
        :type url: str
        :return: Publication instance or None if not supported
        """
        
        # Checks if module supports the 'url' and 
        # returns a Publication if it does.
        for fetcher_class in InputInterface.fetcher_classes:
            if fetcher_class.can_use_url(url):
                return fetcher_class.get_pub_light(url)
            
        # No Module for given url was found
        raise ValueError("'{}' is not supported".format(url))
    
    def get_supported_fetchers(self):
        # print(self.fetcher_classes[0].__name__) Useless right now, 
        # because all classes are called the same
        return [a.__name__ for a in self.fetcher_classes]

    def import_fetcher_classes(self):
        """
        Searches in 'get', if there are [a-z]*.py modules (specific Fetchers)
        and tries to import them.
        Saves found modules in 'fetcher_files'.
        """

        # Path to 'get'-package
        self.get_path = '{}/get'.format(pathlib.Path(__file__).parent.resolve())
        
        # Searches for modules with given Pattern
        fetcher_file_names=[]
        for file in next(walk(self.get_path), (None, None, []))[2]:
            if re.match(r'[a-z]+.py', file) is not None:
                fetcher_file_names.append(file)

        # Tries to import those modules and saves their 'Fetcher'-class
        for file in fetcher_file_names:
            try:
                fetcher_class = importlib.import_module("input.get.{}".format(file[:-3]))
                try:
                    self.fetcher_classes.append(fetcher_class.__getattribute__('Fetcher'))
                except Exception as error:
                    ImportError("Module '{}' does not have a 'Fetcher'-class".format(file[:-3]))
            except Exception:
                raise ImportError("Module '{}' can not be imported".format(file[:-3]))
