#!/usr/bin/env python3

# this is needed for typing pre python 3.9, this maybe as an large Overhead
from typing import Any, List


class Publication:
    """
        Represents a Publications
    """
    def __init__(self, doi_url: str, title: str \
                 , contributors: List[str], journal: str \
                 , publication_date: str, subjects: List[str]\
                 , references: List[Any] = None, citations: List[Any] = None ):
        """
        Parameters
        ----------
        :param doi_url: doi_url of the publication
        :type doi_url: str
        :param title: title of the publication
        :type title: str
        :param contributors:list of all contributors
        :type contributors: list[]
        :param published: date of release
        :type published: str
        :param subjects: the subject of the Publication
        :type subjects: List[str]
        :param references: the Citation which is been referenced by this Publication 
        :type references: List[Any]
        :param citations: the Citation which references this Publication
        :type citations: List[Any]
        :return: None
        """
        self.doi_url = doi_url
        self.title = title
        self.contributors = contributors
        self.journal = journal
        self.publication_date = publication_date
        self.subjects = subjects
        if references is None:
            self.references = []
        else:
            self.references = references
        if citations is None:
            self.citations = []
        else: 
            self.citations = citations
        
        # For the 'Verarbeitungsgruppe'
        self.group = None

    def __str__(self) -> str:
        return ("Title:        {}\n"
                "Doi-url:      {}\n"
                "Authors:      {}\n"
                "Journal:      {}\n"
                "Published on: {}\n"
                "Subjects:     {}\n"
                "References:   \n{}\n"
                "Citations:    \n{}")\
                .format(self.title, self.doi_url, ", ".join(self.contributors)
                        , self.journal, self.publication_date
                        , ", ".join(self.subjects)
                        , "\n".join(self.get_citation_string(self.references))
                        , "\n".join(self.get_citation_string(self.citations)))

    @staticmethod
    def get_citation_string(citations):
        if citations == []:
            return ["None"]
        else:
            citation_string = []
            for citation in citations:
                citation_string.append(citation.__str__())
        return citation_string

    def add_citations(self, citation) -> None:
        """
        Appends a list of Citations or Citation to self.citations.

        Parameter
        ---------
        :param citation: Citation or Reference of the Publication
        :type citation: Citation or list[Citation]
        :return: self.citations
        """
        if type(citation) is Citation:
            self.citations.append(citation)

        # Checks if 'citation' is a list of Citations
        elif type(citation) is list:
            for _cit in citation:
                if type(_cit) is Citation:
                    self.citations.append(_cit)
                else:
                    raise TypeError("_set_citation expects Citations or List of Citations, not: '{}'"
                                    .format(type(_cit)))
        else:
            raise TypeError("_set_citation expects Citations or List of Citations, not: '{}'"
                            .format(type(citation)))

        return self.citations

    def __eq__(self, other) -> bool:
        """ Compares the unique doi_url of two Publications"""
        if type(self)==type(other):
            return self.doi_url == other.doi_url
        return False


class Citation:
    def __init__(self, doi_url: str, title: str \
                , journal: str, contributors: List[str] \
                , cit_type: str = "Citation"):
        """
        Parameters
        ----------
        :param doi_url: doi_url of the publication
        :type doi_url: str
        :param title: title of the publication
        :type title: str
        :param contributors: list of all contributors
        :type contributors: List[str]
        :param cit_type: Specifies if Reference or Citation
        :type cit_type: str
        :return: None
        """

        self.title = title
        self.doi_url = doi_url
        self.journal = journal
        self.contributors = contributors
        self.cit_type = cit_type

    def __str__(self) -> str:
        return ("\t{}-Title:        {}\n"
                "\t{}-Doi:          {}\n"
                "\t{}-Journal:      {}\n"
                "\t{}-Contributors: {}\n")\
                .format(self.cit_type, self.title
                      , self.cit_type, self.doi_url
                      , self.cit_type, self.journal
                      , self.cit_type, ", ".join(self.contributors))
