# Projekt CiS-Projekt 2021/22

Input-Package to fetch publication information with a given url.

## Usage/Examples

```python
from input.interface import InputInterface as Input
from input.publication import Publication

def main(url):
    inter = Input()
    try:
        pub = inter.get_publication(url)
    except Exception as error:
        raise error

    print(pub)
    pub.title = "Cool new Title"
    print(pub)

if __name__ == "__main__":
    main("https://doi.org/10.1021/acs.chemrev.8b00728")
```

The expected results of calling this methode are:
| Input-Url | Result    |
|-----------|-----------|
| supported & correct| A publication Instance |
| supported & uncorrect| ValueError|
| not supported | ValueError|

Supported Url are urls, which comply with the url-pattern of supported Journals.  

### Supported Journals:

- ACS-Journals
- (Nature-Journals)

## Testing

``` c
python -m unittest input/test/<file.py> -v
# for all tests in directory
python -m unittest discover input/test -v
```
## Authors
- Florian Jochens
- Sam Ockenden
- Julius Schenk